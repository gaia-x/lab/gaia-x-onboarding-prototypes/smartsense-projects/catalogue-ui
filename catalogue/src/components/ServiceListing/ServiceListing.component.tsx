/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useEffect, useState } from 'react'
import { Virtuoso } from 'react-virtuoso'
import { CatalogueCard, ProgressBar } from '@gaia-x-frontend/components-lib'
import { SearchServices } from '../SearchServices'
import { ServiceDetail as ServiceDetailComp } from '../ServiceDetail'
import {
  getServiceDetailAPI,
  getServiceListAPI,
} from '@catalogue/api/catalogue.api'
import {
  ServiceDetail as ServiceDetailSkeleton,
  SortBar as SortBarSkeleton,
  CatalogueList as CatalogueSkeleton,
} from '../Skeleton'
import { SortBar } from '../SortBar'
import { SortOrderHelper } from '@catalogue/utility/helper'
import ServiceListingStyle from './ServiceListing.module.scss'
import {
  Content,
  Query,
  ServiceDetail,
  ServiceListRequest,
} from '@catalogue/models/Catalogue.model'

const ServiceListing = () => {
  const [data, setData] = useState<Content[]>([])
  const [page, setPage] = useState<number>(0)
  const [maxPage, setMaxPage] = useState<number>(1)
  const [description, setDescription] = useState<ServiceDetail>()
  const [totalRecords, setTotalRecords] = useState<number>()
  const [activeCard, setActiveCard] = useState<string>()
  const [loadingList, setLoadingList] = useState<boolean>(true)
  const [loadingDetails, setLoadingDetails] = useState<boolean>(true)
  const [sortOrder, setSortOrder] = useState<
    | {
        column: string
        sortType: string
      }[]
    | undefined
  >([
    {
      column: 'createdAt',
      sortType: 'DESC',
    },
  ])
  const [query, setQuery] = useState<undefined | Query>()
  useEffect(() => {
    if (page < maxPage && page != 0) {
      getServiceList()
    }
  }, [page, maxPage])

  useEffect(() => {
    if (!description && data.length > 0) {
      getServiceDetail(data[0].id)
    }
  }, [data, description])

  useEffect(() => {
    getServiceList()
  }, [sortOrder, query])

  const onSortChange = (value: number) => {
    setData([])
    setLoadingList(true)
    setLoadingDetails(true)
    setDescription(undefined)
    setPage(0)
    const sortQuery = SortOrderHelper(value)
    setSortOrder(sortQuery)
  }
  const onQueryChange = (relation: Query) => {
    setQuery({ ...relation })
    setLoadingList(true)
    setLoadingDetails(true)
    setDescription(undefined)
    setData([])
    setPage(0)
  }
  const onResetQuery = () => {
    setPage(0)
    setQuery(undefined)
    setLoadingList(true)
    setLoadingDetails(true)
    setDescription(undefined)
    setData([])
  }
  const getServiceList = () => {
    const searchQuery: ServiceListRequest = {
      page: page,
      size: 10,
      query: query,
    }
    if (sortOrder) {
      searchQuery.sort = sortOrder
    }
    getServiceListAPI(searchQuery)
      .then((res) => {
        setData((prevData) => [...prevData, ...res.content])
        if (page == 0) {
          setMaxPage(res.totalPages)
          setTotalRecords(res.totalElements)
        }
        if (res.content.length == 0) {
          setLoadingDetails(false)
        }
        setLoadingList(false)
      })
      .catch(() => {
        setLoadingList(false)
        setLoadingDetails(false)
      })
  }

  const loadMore = useCallback(() => {
    setPage((prePage) => prePage + 1)
  }, [])

  const getServiceDetail = (id: string) => {
    getServiceDetailAPI(id)
      .then((res) => {
        setActiveCard(id)
        setDescription(res.payload)
        setLoadingDetails(false)
      })
      .catch(() => {
        // console.log(err)
      })
  }

  const Footer = () => {
    return (
      <div className="flex justify-center pt-[3rem] pb-[3rem] ">
        {page < maxPage ? <ProgressBar /> : null}
      </div>
    )
  }
  return (
    <div className={ServiceListingStyle.serviceListingPage}>
      <div
        className={
          ServiceListingStyle.searchBar +
          ' h-[9rem] flex content-center items-center relative justify-center'
        }
      >
        <SearchServices
          onQueryChange={onQueryChange}
          onResetQuery={onResetQuery}
        />
      </div>
      <div
        className={
          ServiceListingStyle.mainContainer +
          ' max-w-screen-xl flex gap-8 pt-[1.2rem] p-8'
        }
      >
        <div
          className={
            ServiceListingStyle.mainContent +
            '  flex flex-col gap-[0.7rem] overflow-y-auto grow w-[100rem] relative'
          }
        >
          <div className={loadingList ? 'hidden' : 'block'}>
            <div
              className={
                ServiceListingStyle.sortBar +
                ' flex shrink-0 mb-[1.5rem] w-[100%] h-[5rem] sticky top-0 rounded-[1rem]'
              }
            >
              <SortBar
                totalRecords={totalRecords}
                onSortChange={onSortChange}
              />
            </div>
          </div>
          <div className={loadingList ? 'block' : 'hidden'}>
            <div
              className={
                ServiceListingStyle.sortBar +
                ' flex shrink-0 mb-[1.5rem] w-[100%] h-[5rem] sticky top-0 rounded-[1rem]'
              }
            >
              <SortBarSkeleton />
            </div>
          </div>

          {loadingList ? (
            <CatalogueSkeleton />
          ) : data.length ? (
            <Virtuoso
              data={data}
              endReached={loadMore}
              itemContent={(index, data) => {
                return (
                  <div
                    key={index}
                    className={' mr-[1rem] mb-[0.8rem]'}
                    onClick={() => {
                      setLoadingDetails(true)
                      getServiceDetail(data.id)
                    }}
                  >
                    <CatalogueCard
                      title={data.name}
                      certificate={data.protectionRegime}
                      owner={data.providedBy}
                      location={data.locations}
                      level={data.labelLevel}
                      showBorder={data.id == activeCard}
                    />
                  </div>
                )
              }}
              components={{ Footer }}
            />
          ) : (
            <div
              className={
                ServiceListingStyle.dataNotFound +
                ' flex flex-col h-[100%] w-[100%]  items-center justify-center'
              }
            >
              <div>Oops!</div>
              <div>No record found. Please try again</div>
            </div>
          )}
        </div>

        <div
          className={
            ServiceListingStyle.descriptionContent + ' overflow-x-auto'
          }
        >
          {loadingDetails || loadingList ? (
            <ServiceDetailSkeleton />
          ) : description ? (
            <ServiceDetailComp serviceDetail={description} />
          ) : (
            <div
              className={
                ServiceListingStyle.dataNotFound +
                ' flex h-[100%] w-[100%] items-center justify-center'
              }
            >
              No Data Found
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
export { ServiceListing }
