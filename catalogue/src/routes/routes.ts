import { VITE_BASE_PATH } from '@catalogue/utility/constant'

const ROUTES_CONST = {
  ROOT: VITE_BASE_PATH,
  CATALOGUE: '',
  SO_DETAILS: 'so-details',
}

export { ROUTES_CONST }
