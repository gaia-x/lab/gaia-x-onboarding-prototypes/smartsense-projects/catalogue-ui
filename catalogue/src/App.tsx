import { RouterProvider } from 'react-router-dom'
import {
  ThemeProvider,
  StyledToastContainer,
} from '@gaia-x-frontend/components-lib'
import { router } from './routes'
import { Suspense } from 'react'
import { ApiLoader, AppLoader } from './components'
import { ErrorBoundary } from 'react-error-boundary'
import 'react-toastify/dist/ReactToastify.css'

function App() {
  return (
    <>
      <Suspense fallback={<AppLoader type="linear" />}>
        <ErrorBoundary
          onError={(e: unknown) => {
            console.log('error', e)
          }}
          fallback={
            <div>
              Something just happened. Please reload/refresh the application
            </div>
          }
        >
          <ApiLoader />
          <StyledToastContainer />
          <ThemeProvider>
            <RouterProvider router={router} />
          </ThemeProvider>
        </ErrorBoundary>
      </Suspense>
    </>
  )
}

export default App
