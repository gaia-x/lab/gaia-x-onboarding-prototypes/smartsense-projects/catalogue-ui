import { Link, NavLink, Outlet } from 'react-router-dom'
import { AppBar } from '@gaia-x-frontend/components-lib'
import { VITE_BASE_PATH, WIZARD_URL } from '@catalogue/utility/constant'
import GaiaXLogo from '../../../src/assets/gaia-x-logo.svg'

const CataloguePage = () => {
  const navigationItems = [
    {
      to: VITE_BASE_PATH,
      label: 'Catalogue',
    },
    {
      to: WIZARD_URL,
      label: 'Create Services',
      external: true,
    },
  ]

  const NavItem = (
    <>
      {navigationItems.map((navigationItem) =>
        navigationItem.external ? (
          <a
            key={navigationItem.to}
            href={navigationItem.to}
            className="navLink"
            target="_blank"
            rel="noopener noreferrer"
          >
            {navigationItem.label}
          </a>
        ) : (
          <NavLink
            key={navigationItem.to}
            to={navigationItem.to}
            className={({ isActive }) =>
              isActive ? 'navLink active' : 'navLink'
            }
          >
            {navigationItem.label}
          </NavLink>
        )
      )}
    </>
  )

  return (
    <div>
      <AppBar headerTabs={NavItem}>
        <Link to={VITE_BASE_PATH}>
          <img src={GaiaXLogo} alt="gaia-x logo" />
        </Link>
      </AppBar>
      <div className={'h-[calc(100vh-9.2rem)]'}>
        <Outlet />
      </div>
    </div>
  )
}
export default CataloguePage
