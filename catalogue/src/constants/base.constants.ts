export enum Environment {
  local = 'http://localhost:4000',
}

export enum APIStatus {
  Pending = 0,
  InProgress = 1,
  Success = 2,
  Failure = 3,
}

export enum SearchType {
  Selector = 0,
  Advanced = 1,
}
